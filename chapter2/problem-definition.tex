\subsection{Author profiling}
There are cases there is no potential list of authors at all, but we still would like to detect some profile dimensions of the anonymous author. We want to exploit the fact that different groups of people use language in a different manner. Instead of assigning an author to the text we try to associate the author with classes defined by dimensions like gender or age interval. Many methods that were originally developed for other authorship attribution variants work well in case of author profiling \cite{juola2006authorship}. Following dimensions of an author profile are considered in literature:

\begin{itemize}
	\item \textbf{gender} - In some languages difference between male-written and female-written pieces of text is obvious due to syntactic peculiarities. For other languages, like English, it is much harder to infer the gender of the author of the text. 
	
	There are suggestions made by some authors that style of formal texts should not differ when it comes to the sex of a writer \cite{juola2006authorship}. Other studies on distinguishing gender of author give opposite results. For example, Koppel in his work \cite{koppel2002automatically} was able to classify documents from the British National Corpus w.r.t. gender with the accuracy of approximately 80\%. 
	
	For men, typical style features for gender discrimination are determiners (\textit{a}, \textit{an}, \textit{the}) and prepositions. Women, on the other hand, tend to use pronouns more frequently \cite{juola2006authorship}. Content features more characteristic for men include words related to technology whereas for female words connected with personal life and relationships \cite{koppel2009computational}.    
	
	\item \textbf{age} - Another popular dimension in author profiling is age. Classes are usually defined as age groups - intervals with lower and upper age bounds provided. Profiling exploits the observation that people use different vocabulary and style as they grow older. Attempts made in the field of discriminating documents with respect to age groups give positive results. For instance, age classification made on corpus composed of blog notes and three age groups (13-17, 23-27, 33-47) yielded 76\% accuracy \cite{koppel2009computational}.
	
	For the English language more frequent presence of determiners and prepositions suggests that the author is older. Younger persons are more likely to omit apostrophes in contractions. The most useful content features for identifying teenagers include words connected with mood and school. For people in their twenties it is social life and work, and for thirties family life \cite{koppel2009computational}.
	
	\item \textbf{native language} - Mother tongue affects the way of using a foreign language. People tend to follow stylistic and grammatical constructs that are present in their native language while omitting patterns they are not familiar with. For instance, Slavic language natives often drop articles \cite{koppel2009computational}.
	
	Al-Rfou \cite{al2012detecting} was able to detect non-native English speakers among Wikipedia comments with $74\%$ accuracy. Brooke \cite{brooke2012robust} achieved $78\%$ performance in classification of the International Corpus of Learner English (7 native languages).
	
	\item \textbf{personality} - Another factor that influences the manner of writing. For example, extroverts use socially related words with positive valence more likely than introverts \cite{iacobelli2011large}. Different personality models may be taken into consideration while discriminating authors. One of the possibilities is Big Five theory that describes human personality as a mix of five following traits: neuroticism, extroversion, openness to experience, agreeableness, and conscientiousness \cite{mccrae1992introduction}.
	
	Iacobelli, Gill, Nowson and Oberlander \cite{iacobelli2011large} classified personality traits on a labeled corpus composed of blog posts written by 3000 bloggers. Their best accuracy ranged from 70.51\% in case of neuroticism to 84.36\% for openness.
	
	\item \textbf{emotional state} - A mood of an author at the moment of text creation. Emotional state distinguishes from personality by its ephemeral character: emotions expressed by a writer may vary even between two consecutive sentences of a text. Emotions are considered in two dimensions: the first one is a category, e.g. happiness, sadness; the second one is intensity, indicating how strong the emotion is.
	
	In their work Aman and Szpakowicz \cite{aman2007identifying} have conducted a classification of sentences in blog posts. They managed to distinguish emotional and non-emotional texts with almost 74\% accuracy. Later on \cite{aman2008using}, they focused on fine-grained emotion classification. They modeled emotional state with six base emotions categories proposed by Ekman \cite{ekman1992argument} (happiness, sadness, anger, disgust, surprise, fear) and no-emotion. They significantly beat baseline scores for each emotion category, with happiness being the easiest one to discern.
	
	\item \textbf{health state} - The way people use language may differ with regard to their mental state. Some linguistic constructs may suggest that an author suffers from a neurodegenerative disease or psychological problems. For example, depressed people tend to use negatively valued words and first-person singular pronouns more frequently \cite{rude2004language}.
	
	Diederich, Al-Ajmi and Yellowlees \cite{diederich2007x} performed an analysis of transcribed speech by 31 patients with clinically diagnosed schizophrenia and 9 control patients. They achieved 77\% accuracy in schizophrenics detection.
\end{itemize}
