In this section, we discuss different approaches to training RBMs and ClassRBM in particular. Firstly, we introduce the notion of datasets and define unsupervised and supervised learning tasks. Then, we present training strategies of Restricted Boltzmann Machines, such as learning for feature detection or training each RBM on a separate class for classification. Next, we focus on three training approaches of ClassRBMs: generative, discriminative and hybrid. Generative training models joint distribution of the input vectors and target classes. Discriminative training models conditional probability distribution. Finally, hybrid training combines generative and discriminative criteria.

\subsection{Datasets}
Machine learning algorithms are data-driven, i.e., they condition their behavior basing on the data from the dataset $\mathcal{D}$ provided for learning. The dataset can be either labeled or unlabeled. The labeled dataset consists of pairs $(\boldsymbol{x}^{(i)},y^{(i)})$, where $\boldsymbol{x}^{(i)}$ denotes $i$-th input vector and $y^{(i)}$ is $i$-th target class assigned to the input: $\mathcal{D}_{labeled} = \{(\boldsymbol{x}^{(i)},y^{(i)})\}_{i=1}^{|\mathcal{D}_{labeled}|}$. Unlabeled dataset contains only input vectors: $\mathcal{D}_{unlabeled} = \{\boldsymbol{x}^{(i)}\}_{i=1}^{|\mathcal{D}_{unlabeled}|}$.

The dataset is partitioned into three subsets, known as training, development, and evaluation set. The training set provides data to fit parameters $\boldsymbol{\theta}$ of the trained model. The development set is devoted to hyperparameter selection and deciding when to finish training to avoid overfitting. The evaluation set is used to assess the final performance of the trained model and to compare it with other algorithms.

\subsection{Training tasks}
Depending on effects we want to obtain and available training samples we can train a model in different ways. There are two basic learning tasks:
\begin{itemize}
	\item \textbf{supervised learning} - Training procedure involves fitting parameters $\boldsymbol{\theta}$ of the model, given labeled set $\mathcal{D}_{labeled}$. During training, the model discovers dependencies between a structure of the input and target classes. The information learned by the model is then used to predict target classes of unseen inputs.
	A cost function $\mathcal{L}$ is defined to give feedback of how well trained the model is performing. The goal of training is to minimize the cost function $\mathcal{L}$.
	\item \textbf{unsupervised learning} - Contrary to supervised learning unlabeled set, $\mathcal{D}_{unlabeled}$, is used. Learning involves discovering natural patterns in the data and finding relationships between different inputs. The model can learn to reduce the dimensionality of data, i.e., to represent input sufficiently well with a lower number of variables.
\end{itemize}

\subsection{Using RBM for feature extraction}
Restricted Boltzmann Machine can be used as a building block in more complex frameworks, where it usually plays a role of a feature detector.

The conditional probabilities of latent variables are interpreted as detected features. They serve either as an input for another RBM (to obtain more generalized features) or some discriminative algorithm, e.g., Support Vector Machines.

\subsection{Training separate RBM for each class}
Another way of Restricted Boltzmann Machine application involves training a separate RBM per each class. For input vector $\boldsymbol{x}$, the probability that the RBM was trained on class $y$ is expressed according to (eq. \ref{eq:energy-based-model-with-visible-and-free-energy-distribution}):
\begin{equation} \label{eq:separate-rbm-free-energy-distribution}
P\left(\boldsymbol{x} \mid y\right) = P\left(\boldsymbol{x}; \boldsymbol{\theta}_y\right) = \frac{e^{-\FreeEnergy\left(\boldsymbol{x}; \boldsymbol{\theta}_y\right)}}{\Partition\left(\boldsymbol{\theta}_y\right)},
\end{equation}
with $\boldsymbol{\theta}_y$ being the parameters of the RBM trained on class $y$.

To predict the class, we use Bayes' theorem. We assume that a priori $\boldsymbol{x}$ is equally likely to belong to any class \cite{schmah2008generative}, i.e., $P(y_1) = P(y_2) = \ldots = P(y_{Y})$. Note that this assumption is not satisfied for the PAN-AP-13 corpus. The conditional probability looks as follows:
\begin{equation} \label{eq:separate-rbm-free-energy-distribution-2}
\begin{split}
P\left(y \mid \boldsymbol{x} \right) &= \frac{P\left(y\right) \cdot P\left(\boldsymbol{x} \mid y\right)}{\sum_{y'} P\left(y'\right) \cdot P\left(\boldsymbol{x} \mid y'\right)} \\
&= \frac{P\left(\boldsymbol{x} \mid y\right)}{\sum_{y'} P\left(\boldsymbol{x} \mid y'\right)} = \\
&= \frac{P\left(\boldsymbol{x}; \boldsymbol{\theta}_y\right)}{\sum_{y'} P\left(\boldsymbol{x}; \boldsymbol{\theta}_{y'}\right)} = \\
&= \frac{\frac{1}{\Partition(\boldsymbol{\theta}_y)} \cdot e^{-\FreeEnergy\left(\boldsymbol{x}; \boldsymbol{\theta}_y\right)}}{\sum_{y'}\frac{1}{\Partition(\boldsymbol{\theta}_{y'})} \cdot e^{-\FreeEnergy\left(\boldsymbol{x}; \boldsymbol{\theta}_{y'}\right)}} \\
&= \frac{e^{-\log \Partition(\boldsymbol{\theta}_y)} \cdot e^{-\FreeEnergy\left(\boldsymbol{x}; \boldsymbol{\theta}_y\right)}}{\sum_{y'}e^{-\log \Partition(\boldsymbol{\theta}_{y'})} \cdot e^{-\FreeEnergy\left(\boldsymbol{x}; \boldsymbol{\theta}_{y'}\right)}} \\
&= \frac{e^{-\FreeEnergy\left(\boldsymbol{x}; \boldsymbol{\theta}_y\right) - \log \Partition(\boldsymbol{\theta}_y)}}{\sum_{y'}e^{-\FreeEnergy\left(\boldsymbol{x}; \boldsymbol{\theta}_{y'}\right) - \log \Partition(\boldsymbol{\theta}_{y'})}}.
\end{split}
\end{equation}

In order to calculate the probability (eq. \ref{eq:separate-rbm-free-energy-distribution-2}), we must know the value of free energy and partition function for each RBM. The free energies can be obtained from formula (\ref{eq:free-energy-rbm}). Unfortunately, partition functions are intractable making the computation of $P(y \mid \boldsymbol{x})$ impossible. In order to overcome the difficulty, we replace unknown partition functions with independent parameters $\hat{Z}_y$ and fit them discriminatively \cite{schmah2008generative, hinton2010practical}:

\begin{equation} \label{eq:separate-rbm-free-energy-distribution-with-parameters}
P\left(y \mid \boldsymbol{x} \right) = \frac{e^{-\FreeEnergy\left(\boldsymbol{x}; \boldsymbol{\theta}_y\right) - \log \hat{Z}_y}}{\sum_{y'}e^{-\FreeEnergy\left(\boldsymbol{x}; \boldsymbol{\theta}_{y'}\right) - \log \hat{Z}_{y'}}}.
\end{equation}

The cost function is defined as negative log conditional probability of the class \cite{schmah2008generative}:

\begin{equation}
\label{eq:discriminative-training-objective}
\mathcal{L}_{disc}(\mathcal{D}) = -\sum_{(\boldsymbol{x},y) \in \mathcal{D}} \log P(y \mid \boldsymbol{x}).
\end{equation}

\subsection{Generative Training}
The obvious choice \cite{larochelle2012learning} of the cost function in generative training is negative log-likelihood of joint probability $P(y, \boldsymbol{x})$:
\begin{equation}
\mathcal{L}_{gen}(\mathcal{D}) = -\sum_{(\boldsymbol{x}, y) \in \mathcal{D}} \log P(y, \boldsymbol{x}).
\end{equation}
For a single point $(\boldsymbol{x},y)$ the gradient of $\log P(y,\boldsymbol{x})$ with respect to parameters $\theta$ has the following form:

\begin{equation}
\begin{split}
\ddphi \log P(y, \boldsymbol{x}) &= \ddphi \log \sum_{\boldsymbol{h}} p(y, \boldsymbol{x}, \boldsymbol{h}) = \ddphi \log(\frac{\sum_{\boldsymbol{h}} \exp(-\Energy(y, \boldsymbol{x}, \boldsymbol{h}))}{\Partition}) \\
&= \ddphi \log(\sum_{\boldsymbol{h}} \exp(-\Energy(y, \boldsymbol{x}, \boldsymbol{h})) - \log(\sum_{y',\boldsymbol{x}',\boldsymbol{h}'} \exp(-\Energy(y',\boldsymbol{x}',\boldsymbol{h}')) \\
&= \frac{\sum_{\boldsymbol{h}} -\exp(-\Energy(y,\boldsymbol{x},\boldsymbol{h})) \ddphi \Energy(y, \boldsymbol{x}, \boldsymbol{h})}{\sum_{\boldsymbol{h}'} \exp(-\Energy(y,\boldsymbol{x},\boldsymbol{h}'))} \\
&\quad -\frac{\sum_{y',\boldsymbol{x}', \boldsymbol{h}'} -\exp(-\Energy(y', \boldsymbol{x}', \boldsymbol{h}')) \ddphi \Energy(y',\boldsymbol{x}', \boldsymbol{h}')}{\sum_{y'', \boldsymbol{x}'', \boldsymbol{h}''} \exp(-\Energy(y'', \boldsymbol{x}'', \boldsymbol{h}''))} \\
&= -\frac{\sum_{\boldsymbol{h}} [\exp(-\Energy(y, \boldsymbol{x}, \boldsymbol{h}))/\Partition] \cdot \ddphi \Energy(y, \boldsymbol{x}, \boldsymbol{h})}{\sum_{\boldsymbol{h}'} [\exp(-\Energy(y, \boldsymbol{x}, \boldsymbol{h}'))/\Partition]} \\
&\quad +\sum_{y',\boldsymbol{x}', \boldsymbol{h}'} \frac{\exp(-\Energy(y', \boldsymbol{x}', \boldsymbol{h}'))}{\sum_{y'', \boldsymbol{x}'', \boldsymbol{h}''} \exp(-\Energy(y'', \boldsymbol{x}'', \boldsymbol{h}''))} \cdot \ddphi \Energy(y', \boldsymbol{x}', \boldsymbol{h}') \\
&= -\sum_{\boldsymbol{h}} \frac{p(y, \boldsymbol{x}, \boldsymbol{h})}{\sum_{\boldsymbol{h}'} p(y, \boldsymbol{x}, \boldsymbol{h}')} \cdot \ddphi \Energy(y,\boldsymbol{x},\boldsymbol{h}) \\
&\quad +\sum_{y',\boldsymbol{x}',\boldsymbol{h}'} p(y',\boldsymbol{x}', \boldsymbol{h}') \cdot \ddphi \Energy(y', \boldsymbol{x}', \boldsymbol{h}') \\
&= -\sum_{\boldsymbol{h}} p(\boldsymbol{h} \mid y, \boldsymbol{x}) \cdot \ddphi \Energy(y, \boldsymbol{x}, \boldsymbol{h}) \\
&\quad +\sum_{y', \boldsymbol{x}', \boldsymbol{h}'} p(y', \boldsymbol{x}', \boldsymbol{h}') \cdot \ddphi \Energy(y', \boldsymbol{x}', \boldsymbol{h}') \\
&= -\amsbb{E}_{\boldsymbol{h} \mid y, \boldsymbol{x}}[\ddphi \Energy(y,\boldsymbol{x},\boldsymbol{h})] + \amsbb{E}_{y', \boldsymbol{x}', \boldsymbol{h}'}[\ddphi \Energy(y', \boldsymbol{x}', \boldsymbol{h}')]\\
\end{split}
\end{equation}

Resulting gradient is a sum of two expectations. The first one iterates over all the possible states of the hidden layer, given visible input and target class, whereas the second one sums over all the possible states of an RBM. Therefore, the first expectation is tractable but the second one is not. Fortunately, a good approximation method, called Contrastive Divergence (CD or CD-k), exists. It involves $k$ steps of Gibbs sampling (hence CD-\textit{k}). For $k$ Gibbs steps and a training pair $(\boldsymbol{x}, y)$ the CD algorithm applied to a ClassRBM looks as follows:
\begin{algorithm}[H]
\caption{Contrastive Divergence with $k$ steps}
\label{alg:contrastive-divergence}
\scriptsize
\begin{algorithmic}
	\STATE $\boldsymbol{x}^{(0)} \leftarrow \boldsymbol{x}$ \COMMENT{take a training pair as a start point}
	\STATE $y^{(0)} \leftarrow y$
	\STATE $\boldsymbol{h}^{(0)} \sim P(\boldsymbol{h} \mid y^{(0)}, \boldsymbol{x}^{(0)})$ \COMMENT{compute conditional probability of $\boldsymbol{h}$ and sample from it}
	\STATE
	\FOR{$i \leftarrow 1$ \TO $k$}
	\STATE $\boldsymbol{x}^{(i)} \sim P(\boldsymbol{x} \mid \boldsymbol{h}^{(i-1)})$ \COMMENT{negative phase of a Gibbs step}
	\STATE $y^{(i)} \sim P(y \mid \boldsymbol{h}^{(i-1)})$ 
	\STATE $\boldsymbol{h}^{(i)} \sim P(\boldsymbol{h} \mid y^{(i)}, \boldsymbol{x}^{(i)})$ \COMMENT{positive phase of a Gibbs step}
	\ENDFOR
	\STATE
	\STATE $\boldsymbol{W} \leftarrow \boldsymbol{W} - \lambda (\boldsymbol{h}^{(k)} {\boldsymbol{x}^{(k)}}^{\mathsf{T}} - \boldsymbol{h}^{(0)} {\boldsymbol{x}^{(0)}}^{\mathsf{T}})$ \COMMENT{update parameters $\boldsymbol{\theta}$ of ClassRBM with gradient approximations}
	\STATE $\boldsymbol{U} \leftarrow \boldsymbol{U} - \lambda (\boldsymbol{h}^{(k)} {\boldsymbol{e}_{y}^{(k)}}^{\mathsf{T}} - \boldsymbol{h}^{(0)} {\boldsymbol{e}_{y}^{(0)}}^{\mathsf{T}})$
	\STATE $\boldsymbol{b} \leftarrow \boldsymbol{b} - \lambda (\boldsymbol{x}^{(k)} - \boldsymbol{x}^{(0)})$
	\STATE $\boldsymbol{c} \leftarrow \boldsymbol{c} - \lambda (\boldsymbol{h}^{(k)} - \boldsymbol{h}^{(0)})$
	\STATE $\boldsymbol{d} \leftarrow \boldsymbol{d} - \lambda (\boldsymbol{e}_{y}^{(k)} - \boldsymbol{e}_{y}^{(0)})$
\end{algorithmic}
\end{algorithm}

Empirical results show that the method is surprisingly effective even for few (usually $k = 1$) sampling steps \cite{hinton2002poe, fischer2010empirical}.

The conditional probability $P(\boldsymbol{h} \mid y, \boldsymbol{x})$ factorizes into $\prod_i P(h_i \mid y, \boldsymbol{x})$:
\begin{equation}
\begin{split}
P(\boldsymbol{h} \mid y, \boldsymbol{x}) &= \frac{P(\boldsymbol{h}, y, \boldsymbol{x})}{P(y, \boldsymbol{x})} = \frac{P(\boldsymbol{h}, y, \boldsymbol{x})}{\sum_{\boldsymbol{h}'} P(\boldsymbol{h}', y, \boldsymbol{x})} \\
&= \frac{\exp(\boldsymbol{h}^{\mathsf{T}}\boldsymbol{W}\boldsymbol{x} + \boldsymbol{b}^{\mathsf{T}}\boldsymbol{x} + \boldsymbol{c}^{\mathsf{T}}\boldsymbol{h} + \boldsymbol{d}^{\mathsf{T}}\boldsymbol{e}_y + \boldsymbol{h}^{\mathsf{T}}\boldsymbol{U}\boldsymbol{e}_y)}{\sum_{\boldsymbol{h}'} \exp({\boldsymbol{h}'}^{\mathsf{T}}\boldsymbol{W}\boldsymbol{x} + \boldsymbol{b}^{\mathsf{T}}\boldsymbol{x} + \boldsymbol{c}^{\mathsf{T}}\boldsymbol{h}' + \boldsymbol{d}^{\mathsf{T}}\boldsymbol{e}_y + {\boldsymbol{h}'}^{\mathsf{T}}\boldsymbol{U}\boldsymbol{e}_y)} \\
&= \frac{\exp(\boldsymbol{h}^{\mathsf{T}}\boldsymbol{W}\boldsymbol{x} + \boldsymbol{c}^{\mathsf{T}}\boldsymbol{h} + \boldsymbol{h}^{\mathsf{T}}\boldsymbol{U}\boldsymbol{e}_y)}{\sum_{\boldsymbol{h}'} \exp({\boldsymbol{h}'}^{\mathsf{T}}\boldsymbol{W}\boldsymbol{x} + \boldsymbol{c}^{\mathsf{T}}\boldsymbol{h}' + {\boldsymbol{h}'}^{\mathsf{T}}\boldsymbol{U}\boldsymbol{e}_y)} \\
&= \frac{\exp(\sum_{i=1}^{H} \left[h_i\boldsymbol{W}_{i\cdot}\boldsymbol{x} + {c}_{i}h_i + h_i\boldsymbol{U}_{i\cdot}\boldsymbol{e}_y\right])}{\sum_{\boldsymbol{h}'} \exp(\sum_{i=1}^{H} \left[ {h}^{'}_i\boldsymbol{W}_{i\cdot}\boldsymbol{x} + {c}_{i}{h}^{'}_i + {h}^{'}_i\boldsymbol{U}_{i\cdot}\boldsymbol{e}_y\right])} \\
&= \frac{\prod_{i=1}^{H} \exp(h_i\boldsymbol{W}_{i\cdot}\boldsymbol{x} + {c}_{i}h_i + h_i\boldsymbol{U}_{i\cdot}\boldsymbol{e}_y)}{\sum_{\boldsymbol{h}'} \prod_{i=1}^{H} \exp({h}^{'}_i\boldsymbol{W}_{i\cdot}\boldsymbol{x} + {c}_{i}{h}^{'}_i + {h}^{'}_i\boldsymbol{U}_{i\cdot}\boldsymbol{e}_y)} \\
&= \frac{\prod_{i=1}^{H} \exp(h_i\boldsymbol{W}_{i\cdot}\boldsymbol{x} + {c}_{i}h_i + h_i\boldsymbol{U}_{i\cdot}\boldsymbol{e}_y)}{\sum_{{h}^{'}_1} \dots \sum_{{h}^{'}_H} \prod_{i=1}^{H} \exp({h}^{'}_i\boldsymbol{W}_{i\cdot}\boldsymbol{x} + {c}_{i}{h}^{'}_i + {h}^{'}_i\boldsymbol{U}_{i\cdot}\boldsymbol{e}_y)} \\
&= \frac{\prod_{i=1}^{H} \exp(h_i\boldsymbol{W}_{i\cdot}\boldsymbol{x} + {c}_{i}h_i + h_i\boldsymbol{U}_{i\cdot}\boldsymbol{e}_y)}{\prod_{i=1}^{H} \big[ \sum_{{h}^{'}_i} \exp({{h}^{'}_i}\boldsymbol{W}_{i\cdot}\boldsymbol{x} + {c}_{i}{h}^{'}_i + {h}^{'}_i\boldsymbol{U}_{i\cdot}\boldsymbol{e}_y) \big]} \\
&= \prod_{i=1}^{H} \frac{\exp(h_i\boldsymbol{W}_{i\cdot}\boldsymbol{x} + {c}_{i}h_i + h_i\boldsymbol{U}_{i\cdot}\boldsymbol{e}_y)}{\sum_{{h}^{'}_i} \exp({{h}^{'}_i}\boldsymbol{W}_{i\cdot}\boldsymbol{x} + {c}_{i}{h}^{'}_i + {h}^{'}_i\boldsymbol{U}_{i\cdot}\boldsymbol{e}_y)} \\
&= \prod_{i=1}^{H} P(h_i \mid y, \boldsymbol{x}) \\
\end{split}
\label{eq:factorize_formula}
\end{equation}

It follows from the above that $P(h_i=1 \mid y, \boldsymbol{x})$ is given by the sigmoid function:
\begin{equation}
	P(h_i=1|y, \boldsymbol{x}) = \sigm(\boldsymbol{W}_{i\cdot}\boldsymbol{x} + c_i + U_{iy}).
\end{equation}

Similarly, $p(\boldsymbol{x} \mid \boldsymbol{h})$ factorizes into $\prod_i P(x_i \mid \boldsymbol{h})$.

%Formula 2.
For ease of notation, let $o_{yi}(\boldsymbol{x})$ stand for the total input from $\boldsymbol{x}$:

\begin{equation}
\label{eq:o}
o_{yi}(\boldsymbol{x}) \defeq \sum_{j=1}^{X} W_{ij}x_j+c_i+U_{iy}.
\end{equation}

Predictions about author profile $y$ of text $\boldsymbol{x}$ (or in general, about class $y$ of point $\boldsymbol{x}$) are made with the help of probability $P(y \mid \boldsymbol{x})$, expressed as:

\begin{equation}
\label{eq:probability-target-class-given-visible}
\begin{split}
P(y \mid \boldsymbol{x}) &= \frac{P(y, \boldsymbol{x})}{P(\boldsymbol{x})} = \frac{\sum_{\boldsymbol{h}} P(y, \boldsymbol{x}, \boldsymbol{h})}{\sum_{y'} \sum_{\boldsymbol{h}'} P(y', \boldsymbol{x}, \boldsymbol{h}')} \\
&= \frac{\sum_{\boldsymbol{h}} \exp({\boldsymbol{h}}^{\mathsf{T}}\boldsymbol{W}\boldsymbol{x} + {\boldsymbol{b}}^{\mathsf{T}}\boldsymbol{x} + {\boldsymbol{c}}^{\mathsf{T}}\boldsymbol{h} + {\boldsymbol{d}}^{\mathsf{T}}\boldsymbol{e}_y + {\boldsymbol{h}}^{\mathsf{T}}\boldsymbol{U}\boldsymbol{e}_y)}{\sum_{y'=1}^{Y} \sum_{\boldsymbol{h}'} \exp({\boldsymbol{h}'}^{\mathsf{T}}\boldsymbol{W}\boldsymbol{x} + \boldsymbol{b}^{\mathsf{T}}\boldsymbol{x} + {\boldsymbol{c}}^{\mathsf{T}}{\boldsymbol{h}'} + {\boldsymbol{d}}^{\mathsf{T}}{\boldsymbol{e}}_{y'} + {\boldsymbol{h}'}^{\mathsf{T}}\boldsymbol{U}{\boldsymbol{e}}_{y'})} \\
&= \frac{\exp({\boldsymbol{b}}^{\mathsf{T}}\boldsymbol{x}) \exp(d_y) \sum_{\boldsymbol{h}} \exp({\boldsymbol{h}}^{\mathsf{T}}\boldsymbol{W}\boldsymbol{x} + {\boldsymbol{c}}^{\mathsf{T}}\boldsymbol{h} + {\boldsymbol{h}}^{\mathsf{T}}\boldsymbol{U}\boldsymbol{e}_y)}{\exp({\boldsymbol{b}}^{\mathsf{T}}\boldsymbol{x}) \sum_{y'=1}^{Y} \exp(d_{y'}) \sum_{\boldsymbol{h}'} \exp({\boldsymbol{h}'}^{\mathsf{T}}\boldsymbol{W}\boldsymbol{x} + {\boldsymbol{c}}^{\mathsf{T}}\boldsymbol{h}' + {\boldsymbol{h}'}^{\mathsf{T}}\boldsymbol{U}{\boldsymbol{e}}_{y'})} \\
&= \frac{\exp(d_y) \sum_{h_1} \dots \sum_{h_H} \exp(\sum_{i=1}^{H} [h_i{\boldsymbol{W}}_{i\cdot}\boldsymbol{x} + c_ih_i +h_i\boldsymbol{U}_{i\cdot}\boldsymbol{e}_y])}{\sum_{y'=1}^{Y} \exp(d_{y'}) \sum_{h^{'}_1} \dots \sum_{h^{'}_H} \exp(\sum_{i=1}^{H} [ {h^{'}_i}{\boldsymbol{W}}_{i\cdot}\boldsymbol{x} + c_ih^{'}_i + h^{'}_i{\boldsymbol{U}}_{i\cdot}{\boldsymbol{e}}_{y'}])} \\
&= \frac{\exp(d_y) \sum_{h_1} \dots \sum_{h_H} \prod_{i=1}^{H} \exp(h_i\boldsymbol{W}_{i\cdot}\boldsymbol{x} + c_ih_i +h_i{\boldsymbol{U}}_{i\cdot}\boldsymbol{e}_y)}{\sum_{y'=1}^{Y} \exp(d_{y'}) \sum_{h^{'}_1} \dots \sum_{h^{'}_H} \prod_{i=1}^{H} \exp({h^{'}_i}{\boldsymbol{W}}_{i\cdot}{\boldsymbol{x}} + c_ih^{'}_i + h^{'}_i{\boldsymbol{U}}_{i\cdot}{\boldsymbol{e}}_{y'})} \\
&= \frac{\exp(d_y) \prod_{i=1}^{H} \sum_{h_i} \exp(h_i{\boldsymbol{W}}_{i\cdot}\boldsymbol{x} + c_ih_i +h_i{\boldsymbol{U}}_{i\cdot}\boldsymbol{e}_y)}{\sum_{y'=1}^{Y} \exp(d_{y'}) \prod_{i=1}^{H} \big[\sum_{h^{'}_i} \exp({h^{'}_i}{\boldsymbol{W}}_{i\cdot}\boldsymbol{x} + c_ih^{'}_i +h^{'}_i{\boldsymbol{U}}_{i\cdot}{\boldsymbol{e}}_{y'})\big]} \\
&= \frac{\exp(d_y) \prod_{i=1}^{H} [1+\exp(\sum_{j=1}^{X} W_{ij}x_j + c_i +U_{iy})]}{\sum_{y'=1}^{Y} \exp(d_{y'}) \prod_{i=1}^{H} \big[1+\exp(\sum_{j=1}^{X} W_{ij}x_j + c_i + U_{i{y'}})\big]} \\
&= \frac{\exp(d_y) \prod_{i=1}^{H} [1+\exp(o_{yi}(\boldsymbol{x}))]}{\sum_{y'=1}^{Y} \exp(d_{y'}) \prod_{i=1}^{H} \big[1+\exp(o_{{y'}i}(\boldsymbol{x}))\big]}. \\
\end{split}
\end{equation}

Formula (\ref{eq:probability-target-class-given-visible}) is a division of product and sum of products with exponentials. This makes the numerator and the denominator prone to arithmetic overflow even for relatively small values of parameters $\boldsymbol{\theta}$ and, in practice, impedes precise computation of the probability.
The problem is mitigated by rewriting the formula to achieve computationally more stable form. Let us transform the nominator (which is the same as the term of summation in the denominator) to numerically more stable form:
\begin{equation}
\label{eq:probability-target-class-given-visible-nominator}
\begin{split}
\exp(d_y) \prod\nolimits_{i=1}^{H} [1+\exp(o_{yi}(\boldsymbol{x}))] &= \exp\Big(\log\big(\exp(d_y) \prod\nolimits_{i=1}^{H} \left[1+\exp(o_{yi}(\boldsymbol{x)})\right]\big)\Big) \\
&= \exp\Big(\log\big(\exp(d_y)\big) + \log\big(\prod\nolimits_{i=1}^{H} [1+\exp(o_{yi}(\boldsymbol{x}))]\big)\Big) \\
&= \exp\Big(d_y + \sum\nolimits_{i=1}^{H} \log\big(1+\exp(o_{yi}(\boldsymbol{x}))\big)\Big) \\
&= \exp\Big(d_y + \sum\nolimits_{i=1}^{H} \softplus\big(o_{yi}(\boldsymbol{x})\big)\Big). \\
\end{split}
\end{equation}
Substituting (eq. \ref{eq:probability-target-class-given-visible-nominator}) into nominator and denominator of $P(y \mid \boldsymbol{x})$ (eq. \ref{eq:probability-target-class-given-visible}) and making final transformations, we obtain:
\begin{equation}
\label{eq:probability-target-class-given-visible-safe}
\begin{split}
P(y \mid \boldsymbol{x}) &= \frac{\exp(d_y) \prod_{i=1}^{H} [1+\exp(o_{yi}(\boldsymbol{x}))]}{\sum_{y'=1}^{Y} \exp(d_{y'}) \prod_{i=1}^{H} \big[1+\exp(o_{{y'}i}(\boldsymbol{x}))\big]} \\
&= \frac{\exp(d_y + \sum_{i=1}^{H} \softplus(o_{yi}(\boldsymbol{x})))}{\sum_{y'=1}^{Y} \exp(d_{y'} + \sum_{i=1}^{H} \softplus(o_{y'i}(\boldsymbol{x})))} \\
&= \frac{\exp(d_y + \sum_{i=1}^{H} \softplus(o_{yi}(\boldsymbol{x})))}{\sum_{y'=1}^{Y} \exp(d_{y'} + \sum_i \softplus(o_{y'i}(\boldsymbol{x})))} \cdot \frac{\exp(-d_y - \sum_{i=1}^{H} \softplus(o_{yi}(\boldsymbol{x})))}{\exp(-d_y - \sum_{i=1}^{H} \softplus(o_{yi}(\boldsymbol{x})))} \\
&= \frac{\exp(d_y + \sum_{i=1}^{H} \softplus(o_{yi}(\boldsymbol{x})) - d_y - \sum_{i=1}^{H} \softplus(o_{yi}(\boldsymbol{x})))}{\sum_{y'=1}^{Y} \exp(d_{y'} + \sum_{i=1}^{H} \softplus(o_{y'i}(\boldsymbol{x})) - d_y - \sum_{i=1}^{H} \softplus(o_{yi}(\boldsymbol{x})))} \\
&= \frac{1}{\sum_{y'=1}^{Y} \exp\big(d_{y'} - d_y  + \sum_{i=1}^{H} \big[\softplus(o_{y'i}(\boldsymbol{x})) - \softplus(o_{yi}(\boldsymbol{x}))\big]\big)}. \\
\end{split}
\end{equation}


\subsection{Discriminative Training}
For discriminative training, the cost function $\mathcal{L}_{disc}$ has usually the following form:
\begin{equation}
\mathcal{L}_{disc}(\mathcal{D}) = -\sum_{(\boldsymbol{x},y) \in \mathcal{D}} \log P(y \mid \boldsymbol{x}).
\end{equation}

A discriminative training procedure ignores the structure of the input and only tries to model the way in which the output depends on the input \cite{hinton2007recognize}. Unlike in the case of generative training computing the exact gradient for the cost function is tractable. 

In the following computations we use the fact that the derivative of the product of functions $f_1, \dots, f_n$ can be expressed as:

\begin{equation}
(f_1 \dots f_n)' = f_1 \dots f_n(\frac{f'_1}{f_1} + \dots + \frac{f'_n}{f_n}) \enspace.
\end{equation}

For a single point $(\boldsymbol{x},y)$ the gradient of $\log P(y \mid \boldsymbol{x})$ with respect to parameters $\theta \in \{W, c, U\}$ is as follows:
\begin{equation}
\label{eq:log-likelihood-conditional-probability-derivative-W-c-U}
\begin{split}
\ddphi \log P(y \mid \boldsymbol{x}) &= \ddphi \sum_i \log\big(1+\exp(\sum_j W_{ij}x_j+c_i+U_{iy})\big) \\
&\quad - \frac{\ddphi \sum_{y'=1}^{Y} \exp(d_{y'}) \prod_{i=1}^{H} \big(1+\exp(\sum_{j=1}^{X} W_{ij}x_j+c_i+U_{iy'})\big)} {\sum_{y'=1}^{Y} \exp(d_{y'}) \prod_{i=1}^{H} \big(1+\exp(\sum_{j=1}^{X} W_{ij}x_j+c_i+U_{iy'})\big)} \\
&= \sum_{i=1}^{H} \frac{\exp(\sum_{j=1}^{X} W_{ij}x_j+c_i+U_{iy}) \cdot \ddphi \big[\sum_{j=1}^{X} W_{ij}x_j+c_i+U_{iy}\big]}{1+\exp(\sum_{j=1}^{X} W_{ij}x_j+c_i+U_{iy})} \\
&\quad - \frac{\sum_{y'=1}^{Y} \exp(d_{y'}) \prod_{i=1}^{H} \big[1+\exp(o_{y'i}(\boldsymbol{x})] \cdot [\sum_{i=1}^{H} \frac{\exp(o_{y'i}(\boldsymbol{x}))}{1+\exp(o_{y'i}(\boldsymbol{x}))} \frac{\partial o_{y'i}(\boldsymbol{x})}{\partial \theta} ]}{\sum_{y'=1}^{Y} \exp(d_{y'}) \prod_{i=1}^{H} \big[1+\exp(o_{y'i}(\boldsymbol{x})]} \\
&= \sum_{i=1}^{H} \sigm(o_{yi}(\boldsymbol{x})) \frac{\partial o_{yi}(\boldsymbol{x})}{\partial \theta} \\
&\quad - \sum_{y'=1}^{Y} p(y' \mid \boldsymbol{x}) \cdot \sum_{i=1}^{H} \sigm(o_{y'i}(\boldsymbol{x}))\frac{\partial o_{y'i}(\boldsymbol{x})}{\partial \theta} \\
&= \sum_{i=1}^{H} \sigm(o_{yi}(\boldsymbol{x})) \frac{\partial o_{yi}(\boldsymbol{x})}{\partial \theta} \\
&\quad - \sum_{i,y'} p(y' \mid \boldsymbol{x}) \cdot \sigm(o_{y'i}(\boldsymbol{x})) \frac{\partial o_{y'i}(\boldsymbol{x})}{\partial \theta}
\end{split}
\end{equation}

Basing on formula (\ref{eq:log-likelihood-conditional-probability-derivative-W-c-U}), let us find exact derivatives with respect to $W_{pq}$, $U_{pq}$, $c_p$ and $d_q$ and exact gradients with respect to $\boldsymbol{W}$, $\boldsymbol{U}$, $\boldsymbol{c}$ and $\boldsymbol{d}$.
Matrix operations are natively supported by Numpy and Theano, thus are
much faster and easier to implement than their scalar counterparts in pure Python.
Moreover, matrix operations can be easily parallelized on GPU.
We exploit the fact that for each vector $\boldsymbol{v}$ of size $q$ and each $m \times n$ matrix $\boldsymbol{A}$ and scalar function $f$, the gradient of $f$ is an element-wise application:

\begin{equation}
\begin{split}
\dddf[\boldsymbol{v}] &= \left(\dddf[v_{1}], \dddf[v_{2}], \cdots, \dddf[v_{q}] \right)
\\ \\
\dddf[\boldsymbol{A}] &= 
\begin{pmatrix}
\dddf[a_{11}] & \dddf[a_{12}] & \cdots & \dddf[a_{1n}] \\
\dddf[a_{21}] & \dddf[a_{22}] & \cdots &\dddf[a_{2n}] \\
\vdots  & \vdots  & \ddots & \vdots  \\
\dddf[a_{m1}] & \dddf[a_{m2}] & \cdots & \dddf[a_{mn}]
\end{pmatrix}
\end{split}
\end{equation}
 
Let us consider a vector composed of sigmoids of functions $o_{yi}(\boldsymbol{x}), i \in \{1, \dots, H\}$ (eq. \ref{eq:o}).
 This vector can be written down in different manners:
\begin{equation}
\label{eq:sigm-o-vector}
\begin{split}
\begin{pmatrix}
\sigm(o_{y1}(\boldsymbol{x})) \\
\sigm(o_{y2}(\boldsymbol{x})) \\
\vdots \\
\sigm(o_{yH}(\boldsymbol{x}))
\end{pmatrix} &=
\sigm \begin{pmatrix}
o_{y1}(\boldsymbol{x}) \\
o_{y2}(\boldsymbol{x}) \\
\vdots \\
o_{yH}(\boldsymbol{x})
\end{pmatrix} \\
&=\sigm
\begin{pmatrix}
\sum_j W_{1j}x_j+c_1+U_{1y} \\
\sum_j W_{2j}x_j+c_2+U_{2y} \\
\vdots \\
\sum_j W_{Hj}x_j+c_H+U_{Hy}
\end{pmatrix} \\
&= \sigm \left(\boldsymbol{W}\boldsymbol{x} + \boldsymbol{c} + \boldsymbol{U_{\cdot y}}\right)
\end{split}
\end{equation}
The matrix of sigmoids of functions $ o_{yi}(\boldsymbol{x}), i \in \{1, \dots, H\}, y \in \{1, \dots, Y\}$ can be expressed as:
\begin{equation}
\begin{split}
\label{eq:sigm-o-matrix}
&\begin{pmatrix}
\sigm(o_{11}(\boldsymbol{x})) & \cdots & \sigm(o_{Y1}(\boldsymbol{x})) \\
\vdots & \ddots & \vdots \\
\sigm(o_{1H}(\boldsymbol{x})) & \cdots & \sigm(o_{YH}(\boldsymbol{x})) \\
\end{pmatrix} = \\
&= \sigm
\begin{pmatrix}
o_{11}(\boldsymbol{x}) & \cdots & o_{Y1}(\boldsymbol{x}) \\
\vdots & \ddots & \vdots \\
o_{1H}(\boldsymbol{x}) & \cdots & o_{YH}(\boldsymbol{x}) \\
\end{pmatrix} \\
&= \sigm
\begin{pmatrix}
\sum_j W_{1j}x_j+c_1+U_{11} & \cdots & \sum_j W_{1j}x_j+c_1+U_{1Y} \\
\vdots & \ddots & \vdots \\
\sum_j W_{Hj}x_j+c_H+U_{H1} & \cdots & \sum_j W_{Hj}x_j+c_H+U_{HY}
\end{pmatrix} \\
&= \sigm \left(\left(\boldsymbol{W}\boldsymbol{x} + \boldsymbol{c}\right)_{p=1}^{Y} + \boldsymbol{U}\right).
\end{split}
\end{equation}

Having (eq. \ref{eq:sigm-o-vector}) and (eq. \ref{eq:sigm-o-matrix}) we can find derivatives of conditional log-likelihood. For element $W_{pq}$ of matrix $\boldsymbol{W}$ equation (\ref{eq:log-likelihood-conditional-probability-derivative-W-c-U}) takes the form:
\begin{equation}
\label{eq:log-likelihood-conditional-probability-derivative-W-start}
\begin{split}
\frac{\partial }{\partial W_{pq}} \log P(y \mid \boldsymbol{x}) &=\sum_{i} \sigm(o_{yi}(\boldsymbol{x})) \frac{\partial o_{yi}(\boldsymbol{x})}{\partial W_{pq}} \\
&\quad - \sum_{i,y'} P(y'|\boldsymbol{x}) \cdot \sigm(o_{y'i}(\boldsymbol{x})) \frac{\partial o_{y'i}(\boldsymbol{x})}{\partial W_{pq}}.
\end{split}
\end{equation}

Lacking derivative $\frac{\partial o_{yi}(\boldsymbol{x})}{\partial W_{pq}}$ can be easily obtained from (eq. \ref{eq:o}):

\begin{equation}
\label{eq:deriative-o-wrt-W}
\begin{split}
\frac{\partial o_{yi}(\boldsymbol{x})}{\partial W_{pq}} &=
\frac{\partial (\sum_j W_{ij}x_j+c_i+U_{iy})}{\partial W_{pq}} \\
&=\frac{\partial (\sum_j W_{ij}x_j)}{\partial W_{pq}} + \frac{\partial c_i}{\partial W_{pq}} + \frac{\partial U_{iy}}{\partial W_{pq}} \\
&=\frac{\partial (W_{i1}x_1 + \dots + W_{iq}x_q + \dots + W_{iX}x_X)}{\partial W_{pq}} \\
&=\frac{\partial W_{i1}x_1}{\partial W_{pq}} + \dots + \frac{\partial W_{iq}x_q}{\partial W_{pq}} + \dots + \frac{\partial W_{iX}x_X}{\partial W_{pq}} \\
&=\frac{\partial W_{iq}x_q}{\partial W_{pq}} = \mathbbm{1}[i=p] x_q
\end{split}
\end{equation}

We now substitute (eq. \ref{eq:deriative-o-wrt-W}) to (eq. \ref{eq:log-likelihood-conditional-probability-derivative-W-start}):
\begin{equation}
\label{eq:log-likelihood-conditional-probability-derivative-W-deriative-substitution}
\begin{split}
\frac{\partial }{\partial W_{pq}} \log P(y \mid \boldsymbol{x}) &=\sum_{i} \sigm(o_{yi}(\boldsymbol{x})) \frac{\partial o_{yi}(\boldsymbol{x})}{\partial W_{pq}} \\
&\quad - \sum_{i,y'} P(y' \mid \boldsymbol{x}) \cdot \sigm(o_{y'i}(\boldsymbol{x})) \frac{\partial o_{y'i}(\boldsymbol{x})}{\partial W_{pq}} \\
&=\sum_{i} \sigm(o_{yi}(\boldsymbol{x})) \cdot \mathbbm{1}[i=p] x_q \\
&\quad - \sum_{i,y'} P(y' \mid \boldsymbol{x}) \cdot \sigm(o_{y'i}(\boldsymbol{x})) \cdot \mathbbm{1}[i=p] x_q \\
&=\sigm(o_{yp}(\boldsymbol{x})) \cdot x_q \\
&\quad - \sum_{y'} P(y' \mid \boldsymbol{x}) \cdot \sigm(o_{y'p}(\boldsymbol{x})) \cdot x_q \\
&=\left(\sigm(o_{yp}(\boldsymbol{x})) - \sum_{y'} P(y' \mid \boldsymbol{x}) \cdot \sigm(o_{y'p}(\boldsymbol{x}))\right) \cdot x_q
\end{split}
\end{equation}
Multiplicand $x_q$ does not depend on $p$ and multiplier (the rest of equation above) does not depend on $q$. It means that the scalar-by-scalar derivative (eq. \ref{eq:log-likelihood-conditional-probability-derivative-W-deriative-substitution}) can be generalized to scalar-by-matrix gradient as an outer product of two vectors:
\begin{equation}
\begin{split}
&\ddd[\boldsymbol{W}] \log P(y \mid \boldsymbol{x}) = 
\begin{pmatrix}
\sigm(o_{y1}(\boldsymbol{x})) - \sum_{y'} P(y' \mid \boldsymbol{x}) \cdot \sigm(o_{y'1}(\boldsymbol{x})) \\
\vdots \\
\sigm(o_{yH}(\boldsymbol{x})) - \sum_{y'} P(y' \mid \boldsymbol{x}) \cdot \sigm(o_{y'H}(\boldsymbol{x}))
\end{pmatrix}
\boldsymbol{x}^{\mathsf{T}} \\
&= \left(
\begin{pmatrix}
\sigm(o_{y1}(\boldsymbol{x})) \\
\vdots \\
\sigm(o_{yH}(\boldsymbol{x}))
\end{pmatrix} -
\begin{pmatrix}
\sum_{y'} P(y' \mid \boldsymbol{x}) \cdot \sigm(o_{y'1}(\boldsymbol{x})) \\
\vdots \\
\sum_{y'} P(y' \mid \boldsymbol{x}) \cdot \sigm(o_{y'H}(\boldsymbol{x}))
\end{pmatrix} \right)
\boldsymbol{x}^{\mathsf{T}} \\
&= \left( 
\begin{pmatrix}
\sigm(o_{y1}(\boldsymbol{x})) \\
\vdots \\
\sigm(o_{yH}(\boldsymbol{x}))
\end{pmatrix} -
\begin{pmatrix}
\sigm(o_{11}(\boldsymbol{x})) & \cdots & \sigm(o_{Y1}(\boldsymbol{x})) \\
\vdots & \ddots & \vdots \\
\sigm(o_{1H}(\boldsymbol{x})) & \cdots & \sigm(o_{YH}(\boldsymbol{x})) \\
\end{pmatrix}
\begin{pmatrix}
P(y=1 \mid \boldsymbol{x}) \\
\vdots \\
P(y=Y \mid \boldsymbol{x})
\end{pmatrix} \right)
\boldsymbol{x}^{\mathsf{T}} \\
&= \left(\sigm \left(\boldsymbol{W}\boldsymbol{x} + \boldsymbol{c} + \boldsymbol{U_{\cdot y}}\right) - \sigm \left(\left(\boldsymbol{W}\boldsymbol{x} + \boldsymbol{c}\right)_{p=1}^Y + \boldsymbol{U}\right) 
\begin{pmatrix}
P(y=1 \mid \boldsymbol{x}) \\
\vdots \\
P(y=Y \mid \boldsymbol{x})
\end{pmatrix}\right)
\boldsymbol{x}^{\mathsf{T}}
\end{split}
\end{equation}
The last line of the above equation is the result of substitution with (eq. \ref{eq:sigm-o-vector}) and (eq.\ref{eq:sigm-o-matrix}).

Let us move on to conditional log-likelihood derivative with respect to an element $U_{pq}$ of matrix $\boldsymbol{U}$. Equation (\ref{eq:log-likelihood-conditional-probability-derivative-W-c-U}) looks as follows:
\begin{equation}
\label{eq:log-likelihood-conditional-probability-derivative-U-start}
\begin{split}
\frac{\partial }{\partial U_{pq}} \log P(y \mid \boldsymbol{x}) &= \sum_{i} \sigm(o_{yi}(x)) \frac{\partial o_{yi}(x)}{\partial U_{pq}} \\
&\quad - \sum_{i,y'} p(y'|x) \cdot \sigm(o_{y'i}(x)) \frac{\partial o_{y'i}(x)}{\partial U_{pq}}
\end{split}
\end{equation}
We need the derivative of $o_{yi}(x)$ (eq. \ref{eq:o}) with respect to $U_{pq}$:
\begin{equation}
\label{eq:deriative-o-wrt-U}
\begin{split}
\frac{\partial o_{yi}(\boldsymbol{x})}{\partial U_{pq}} &=
\frac{\partial (\sum_j W_{ij}x_j+c_i+U_{iy})}{\partial U_{pq}} \\
&=\frac{\partial (\sum_j W_{ij}x_j)}{\partial U_{pq}} + \frac{\partial c_i}{\partial U_{pq}} + \frac{\partial U_{iy}}{\partial U_{pq}} \\
&=\frac{\partial U_{iy}}{\partial U_{pq}} = \mathbbm{1}\left[ i=p \right] \cdot \mathbbm{1}\left[ y=q \right]
\end{split}
\end{equation}
After substitution of (eq. \ref{eq:deriative-o-wrt-U}) to (eq.\ref{eq:log-likelihood-conditional-probability-derivative-U-start}):
\begin{equation}
\label{eq:log-likelihood-conditional-probability-derivative-U}
\begin{split}
\frac{\partial }{\partial U_{pq}} \log P(y \mid \boldsymbol{x}) &= \sum_{i} \sigm(o_{yi}(\boldsymbol{x})) \frac{\partial o_{yi}(\boldsymbol{x})}{\partial U_{pq}} \\
&\quad - \sum_{i,y'} P(y' \mid \boldsymbol{x}) \cdot \sigm(o_{y'i}(\boldsymbol{x})) \frac{\partial o_{y'i}(\boldsymbol{x})}{\partial U_{pq}} = \\
&= \sum_{i} \sigm(o_{yi}(\boldsymbol{x})) \cdot \mathbbm{1}\left[i=p\right] \cdot \mathbbm{1}{\left[y=q \right]} \\
&\quad - \sum_{i,y'} P(y' \mid \boldsymbol{x}) \cdot \sigm(o_{y'i}(\boldsymbol{x})) \cdot \mathbbm{1}{\left[ i=p \right]}\cdot\mathbbm{1}{\left[ y'=q \right]} = \\
&= \sigm(o_{yp}(\boldsymbol{x})) \cdot \mathbbm{1}{\left[ y=q \right]} - P(q \mid \boldsymbol{x}) \cdot \sigm(o_{qp}(\boldsymbol{x})) = \\
&= \sigm(o_{qp}(\boldsymbol{x})) \cdot \mathbbm{1}{\left[ y=q \right]} - P(q \mid \boldsymbol{x}) \cdot \sigm(o_{qp}(\boldsymbol{x})) = \\
&= \sigm(o_{qp}(\boldsymbol{x})) \cdot \left(\mathbbm{1}{\left[ y=q \right]} - P(q \mid \boldsymbol{x})\right)
\end{split}
\end{equation}
Equation (\ref{eq:log-likelihood-conditional-probability-derivative-U}) generalized to matrix form looks as follows ($\odot$ denotes the Hadamard product):
\begin{equation}
\begin{split}
&\ddd[\boldsymbol{U}] \log P(y \mid \boldsymbol{x}) =
\begin{pmatrix}
\ddd[U_{11}] \log P(y \mid \boldsymbol{x}) & \cdots & \ddd[U_{1Y}] \log P(y \mid \boldsymbol{x}) \\
\vdots & \ddots & \vdots \\
\ddd[U_{H1}] \log P(y \mid \boldsymbol{x}) & \cdots & \ddd[U_{HY}] \log P(y \mid \boldsymbol{x})
\end{pmatrix} = \\
&= \begin{pmatrix}
\sigm(o_{11}(\boldsymbol{x})) \left(\mathbbm{1}{[y=1]} - P(y=1 \mid \boldsymbol{x})\right) & \cdots & \sigm(o_{Y1}(\boldsymbol{x})) \left(\mathbbm{1}{[y=Y]} - P(y=Y \mid \boldsymbol{x})\right) \\
\vdots & \ddots & \vdots \\
\sigm(o_{1H}(\boldsymbol{x}))\left(\mathbbm{1}{[y=1]} - P(y=1 \mid \boldsymbol{x})\right) & \cdots & \sigm(o_{YH}(\boldsymbol{x})) \left(\mathbbm{1}{[y=Y]} - P(y=Y \mid \boldsymbol{x})\right)
\end{pmatrix} = \\
&= \begin{pmatrix}
\sigm(o_{11}(\boldsymbol{x})) & \cdots & \sigm(o_{Y1}(\boldsymbol{x})) \\
\vdots & \ddots & \vdots \\
\sigm(o_{1H}(\boldsymbol{x})) & \cdots & \sigm(o_{YH}(\boldsymbol{x})) 
\end{pmatrix} 
\odot \\
& \quad \odot
\begin{pmatrix}
\mathbbm{1}{[y=1]} - P(y=1 \mid \boldsymbol{x}) & \cdots & \mathbbm{1}{[y=Y]} - P(y=Y \mid \boldsymbol{x}) \\
\vdots & \ddots & \vdots \\
\mathbbm{1}{[y=1]} - P(y=1 \mid \boldsymbol{x}) & \cdots & \mathbbm{1}{[y=Y]} - P(y=Y \mid \boldsymbol{x})
\end{pmatrix} \\ 
\end{split}
\end{equation}
Let us consider derivative (eq. \ref{eq:log-likelihood-conditional-probability-derivative-W-c-U}) with respect to $c_p$:
\begin{equation}
\label{eq:log-likelihood-conditional-probability-derivative-c-start}
\begin{split}
\frac{\partial }{\partial c_p} \log P(y \mid \boldsymbol{x}) &=\sum_{i} \sigm(o_{yi}(\boldsymbol{x})) \frac{\partial o_{yi}(\boldsymbol{x})}{\partial c_p} \\
&\quad - \sum_{i,y'} P(y' \mid \boldsymbol{x}) \cdot \sigm(o_{y'i}(\boldsymbol{x})) \frac{\partial o_{y'i}(\boldsymbol{x})}{\partial c_p}
\end{split}
\end{equation}
The derivative of $o_{yi}(\boldsymbol{x})$ w.r.t. $c_p$ takes the form:
\begin{equation}
\label{eq:deriative-o-wrt-c}
\begin{split}
\frac{\partial o_{yi}(\boldsymbol{x})}{\partial c_p} &=
\frac{\partial (\sum_j W_{ij}x_j+c_i+U_{iy})}{\partial c_p} \\
&=\frac{\partial (\sum_j W_{ij}x_j)}{\partial c_p} + \frac{\partial c_i}{\partial c_p} + \frac{\partial U_{iy}}{\partial c_p} \\
&=\frac{\partial c_i}{\partial c_p} = \mathbbm{1}{[i=p]}
\end{split}
\end{equation}
Substituting (eq. \ref{eq:deriative-o-wrt-c}) to (eq. \ref{eq:log-likelihood-conditional-probability-derivative-c-start}) we get:
\begin{equation}
\label{eq:log-likelihood-conditional-probability-derivative-c}
\begin{split}
\frac{\partial }{\partial c_p} \log P(y \mid \boldsymbol{x}) &=\sum_{i} \sigm(o_{yi}(\boldsymbol{x})) \cdot \mathbbm{1}{[i=p]} \\
&\quad - \sum_{i,y'} P(y' \mid \boldsymbol{x}) \cdot \sigm(o_{y'i}(\boldsymbol{x})) \cdot \mathbbm{1}{[i=p]} \\
&= \sigm(o_{yp}(\boldsymbol{x})) - \sum_{y'} P(y' \mid \boldsymbol{x}) \cdot \sigm(o_{y'p}(\boldsymbol{x})) \\
\end{split}
\end{equation}
Conditional log-likelihood gradient with respect to vector $\boldsymbol{c}$ is then as follows:
\begin{equation}
\begin{split}
\frac{\partial }{\partial \boldsymbol{c}} \log P(y \mid \boldsymbol{x}) 
&= 
\begin{pmatrix}
\sigm(o_{y1}(\boldsymbol{x})) \\
\vdots \\
\sigm(o_{yH}(\boldsymbol{x}))
\end{pmatrix} \\
&\quad - 
\begin{pmatrix}
\sum_{y'} P(y' \mid \boldsymbol{x}) \cdot \sigm(o_{y'1}(\boldsymbol{x})) \\
\vdots \\
\sum_{y'} P(y' \mid \boldsymbol{x}) \cdot \sigm(o_{y'H}(\boldsymbol{x}))
\end{pmatrix} \\
&= 
\begin{pmatrix}
\sigm(o_{y1}(\boldsymbol{x})) \\
\vdots \\
\sigm(o_{yH}(\boldsymbol{x}))
\end{pmatrix} \\
&\quad - 
\begin{pmatrix}
\sigm(o_{11}(\boldsymbol{x})) & \cdots & \sigm(o_{Y1}(\boldsymbol{x})) \\
\vdots & \ddots & \vdots \\
\sigm(o_{1H}(\boldsymbol{x})) & \cdots & \sigm(o_{YH}(\boldsymbol{x})) \\
\end{pmatrix}
\begin{pmatrix}
P(y=1 \mid \boldsymbol{x}) \\
\vdots \\
P(y=Y \mid \boldsymbol{x})
\end{pmatrix} \\
&= \sigm \left(\boldsymbol{W}\boldsymbol{x} + \boldsymbol{c} + \boldsymbol{U_{\cdot y}}\right) - 
\sigm \left(\left(\boldsymbol{W}\boldsymbol{x} + \boldsymbol{c}\right)_{p=1}^{Y} + \boldsymbol{U}\right)
\begin{pmatrix}
P(y=1 \mid \boldsymbol{x}) \\
\vdots \\
P(y=Y \mid \boldsymbol{x})
\end{pmatrix}
\end{split}
\end{equation}

For $\theta = d_q$ derivative of $\log P(y \mid \boldsymbol{x})$ takes the form:

\begin{equation}
\label{eq:log-likelihood-conditional-probability-derivative-d}
\begin{split}
\ddd[d_q] \log P(y \mid \boldsymbol{x}) &= \ddd[d_q] \log \Big( \exp(d_y) \prod_{i=1}^{H} [1+\exp(\sum_{j=1}^{X} W_{ij}x_j + c_i +U_{iy})] \Big) \\
&- \ddd[d_q] \log \Big( \sum_{y'=1}^{Y} \exp(d_{y'}) \prod_{i=1}^{H} \big[1+\exp(\sum_{j=1}^{X} W_{ij}x_j + c_i + U_{i{y'}})\big] \Big) \\
&= \frac{\mathbbm{1}{[q=y]} \exp(d_y) \prod_{i=1}^{H} [1+\exp(\sum_{j=1}^{X} W_{ij}x_j + c_i +U_{iy})]}{\exp(d_y) \prod_{i=1}^{H} [1+\exp(\sum_{j=1}^{X} W_{ij}x_j + c_i +U_{iy})]} \\
&- \frac{\exp(d_q) \prod_{i=1}^{H} \big[1+\exp(\sum_{j=1}^{X} W_{ij}x_j + c_i + U_{iq})\big]}{\sum_{y'=1}^{Y} \exp(d_{y'}) \prod_{i=1}^{H} \big[1+\exp(\sum_j W_{ij}x_j + c_i + U_{i{y'}})\big]} \\
&= \mathbbm{1}{[q=y]} - P(q \mid \boldsymbol{x}) \\
\end{split}
\end{equation}

For parameter $\theta = \boldsymbol{d}$, the gradient of $\log P (y \mid \boldsymbol{x})$ has the following form:
\begin{equation}
\label{eq:log-likelihood-conditional-probability-derivative-d-vector}
\begin{split}
\ddd[\boldsymbol{d}] \log P(y \mid \boldsymbol{x}) &= \begin{pmatrix}
\mathbbm{1}{[y=1]} - P(y=1 \mid \boldsymbol{x}) \\
\mathbbm{1}{[y=2]} - P(y=2 \mid \boldsymbol{x}) \\
\vdots \\
\mathbbm{1}{[y=Y]} - P(y=Y \mid \boldsymbol{x}) \\
\end{pmatrix}
\end{split}
\end{equation}

For $\theta = \boldsymbol{b}$ we have simply $\ddd[\boldsymbol{b}] \log P(y \mid \boldsymbol{x}) = 0$, as $P(y \mid \boldsymbol{x})$ does not depend on bias $\textbf{b}$.

\subsection{Hybrid Training}
Hybrid training combines discriminative and generative approaches:
\begin{equation}
\label{eq:hybrid-training-objective}
\mathcal{L}_{hybrid}(\mathcal{D}) = (1-\alpha)\mathcal{L}_{disc}(\mathcal{D}) + \alpha\mathcal{L}_{gen}(\mathcal{D}),
\end{equation}
where hyperparameter $\alpha$ is a weight controlling proportion between contribution of discriminative and generative terms to the overall cost function. Thus, both joint and conditional probability distributions are modeled at the same time. Training procedure applies contrastive divergence to the generative part and stochastic gradient descent to the discriminative part.