Energy-based models associate scalar energy to each configuration of the variables of interest \cite{bengio2009learning}. Let us introduce energy function, $E\left(\boldsymbol{x}; \boldsymbol{\theta}\right)$, that takes a vector of observed variables $\boldsymbol{x}$ (e.g. word occurrences in a document or pixel intensities of an image) and, given a set of model parameters $\boldsymbol{\theta}$, yields a scalar value.

Given energy function, we define a probability distribution:
\begin{equation} \label{eq:energy-based-model-distribution}
	P\left(\boldsymbol{x}; \boldsymbol{\theta}\right) = \frac{e^{-\Energy\left(\boldsymbol{x}; \boldsymbol{\theta}\right)}} {\Partition\left(\boldsymbol{\theta}\right)},
\end{equation}
where $\Partition$ is so-called \textit{partition function}. The purpose of the partition function is to ensure the sum of all possible outcomes adds up to 1. Therefore, $\Partition$ is defined as a sum over the input space:
\begin{equation} \label{eq:energy-based-model-partition-function}
	\Partition\left(\boldsymbol{\theta}\right) = \sum_{\boldsymbol{x}} e^{-\Energy\left(\boldsymbol{x}; \boldsymbol{\theta}\right)}.
\end{equation}

Linearly decreasing energy associated with the input increases the probability $P\left(\boldsymbol{x}; \boldsymbol{\theta}\right)$ exponentially (eq. \ref{eq:energy-based-model-distribution}). Thus, training energy-based model involves modifying parameters $\boldsymbol{\theta}$ in the way energy function $\Energy(\boldsymbol{x}; \boldsymbol{\theta})$ yields lower values for inputs from a training set and higher values for other (undesirable) inputs.


Sometimes we want to increase the capacity (i.e., the ability to simulate more complex functions) of our model. In order to do that we extend it by adding a vector of latent variables $\boldsymbol{h}$, which are never directly observed. Probability distribution becomes then
\begin{equation} \label{eq:energy-based-model-with-visible-and-hidden-distribution}
	P\left(\boldsymbol{x}, \boldsymbol{h}; \boldsymbol{\theta}\right) = \frac{e^{-\Energy\left(\boldsymbol{x}, \boldsymbol{h}; \boldsymbol{\theta}\right)}} {\Partition\left(\boldsymbol{\theta}\right)}.
\end{equation}

Partition function sums over all possible pairs of observed and hidden vectors:
\begin{equation} \label{eq:energy-based-model-partition-function-with-hidden}
\Partition\left(\boldsymbol{\theta}\right) = \sum_{\boldsymbol{x}, \boldsymbol{h}} e^{-\Energy\left(\boldsymbol{x}, \boldsymbol{h}; \boldsymbol{\theta}\right)}.
\end{equation}

Since we are interested in a distribution w.r.t observed variables only, we sum over all the possible values of the hidden vector:
\begin{equation} \label{eq:energy-based-model-with-visible-distribution}
	P\left(\boldsymbol{x}; \boldsymbol{\theta}\right) = \sum_{\boldsymbol{h}} \frac{e^{-\Energy\left(\boldsymbol{x}, \boldsymbol{h}; \boldsymbol{\theta}\right)}} {\Partition\left(\boldsymbol{\theta}\right)}.
\end{equation}

Formulation above is often expressed with the concept of free energy, $\FreeEnergy(\boldsymbol{x}; \boldsymbol{\theta})$:
	\begin{equation} \label{eq:energy-based-model-with-visible-and-free-energy-distribution}
	P\left(\boldsymbol{x}; \boldsymbol{\theta}\right) = \frac{e^{-\FreeEnergy\left(\boldsymbol{x}; \boldsymbol{\theta}\right)}} {\Partition\left(\boldsymbol{\theta}\right)},
\end{equation}
where
\begin{equation} \label{eq:free-energy}
	\FreeEnergy\left(\boldsymbol{x}; \boldsymbol{\theta}\right) = -\log\sum_{\boldsymbol{h}}e^{-\Energy\left(\boldsymbol{x},\boldsymbol{h}; \boldsymbol{\theta}\right)}.
\end{equation}