Classification Restricted Boltzmann Machine for sets (ClassSetRBM) is a generalization of ClassRBM, which performs multiclass classification of an unordered set of vectors \cite{louradour2011classification}. As opposed to RBM and ClassRBM, ClassSetRBM deals well with variable-size input. In practice, it can be applied when making data fixed-size is infeasible or results in the loss of precious information about the input structure. Typical use cases cover classification of document sets or segmented images. 

This type of RBMs was not examined during author profiling experiments but is presented here to give a complete overview of RBM architectures used for classification problems.

A more traditional approach to sets classification is called multiple-instance learning (MIL). MIL makes the assumption that a set belongs to a class if at least one element of the set belongs to that class. An advantage of ClassSetRBMs is that they do not impose such restrictions: vectors may provide partial information about class membership \cite{louradour2011classification}.

ClassSetRBM models the distribution of set of vectors $\textbf{X} = \{\textbf{x}^{(1)}, \dots, \textbf{x}^{(|\textbf{X}|)} \}$ and the target vector $\textbf{y}$. It enforces a constraint that each vector of a set is connected to a separate copy of a hidden layer. Hence, the number of hidden layers is strictly conditioned on the size of the input set. Each $\boldsymbol{x}^{(s)}$ is connected to respective hidden layer $\boldsymbol{h}^{(s)}$. Various alternatives can be proposed to define interactions between hidden layers and the target vector $\textbf{y}$. Two of them are called ClassSetRBM with Mutually Exclusive Hidden Units and ClassSetRBM with Redundant Evidence.

\subsection{ClassSetRBM with Mutually Exclusive Hidden Units (ClassSetRBM\textsuperscript{XOR})}
ClassSetRBM\textsuperscript{XOR} assumes that the data has mutually exclusive character. If a feature exists in one input vector then it does not exist in any other vector. This condition is mathematically expressed as:

\begin{equation}
\sum_{s=1}^{|\boldsymbol{X}|} h_j^{(s)} \in \{0, 1\},
\end{equation}

for each $j$-th element of vector $\boldsymbol{h}^{(s)}$.

\input{chapter3/classification-restricted-boltzmann-machine-xor-figure}

Energy function is defined as follows:
\begin{equation}
\Energy\left(y, \boldsymbol{X}, \boldsymbol{H}; \boldsymbol{\theta}\right) =  -\boldsymbol{d}^\mathsf{T} \boldsymbol{e}_y - \sum_{s} \boldsymbol{b}^\mathsf{T} \boldsymbol{x}^{(s)} - \sum_{s} \boldsymbol{c}^\mathsf{T} \boldsymbol{h}^{(s)} - \sum_{s} \left( {\boldsymbol{h}^{(s)}}^\mathsf{T}\boldsymbol{W}{\boldsymbol{x}^{(s)}} + {\boldsymbol{h}^{(s)}}^\mathsf{T}\boldsymbol{U}\boldsymbol{e}_y \right).
\end{equation}

Weight matrices $\boldsymbol{W}$, $\boldsymbol{U}$ are shared between all visible, target class and hidden layers. Bias $\boldsymbol{c}$ is reused between all hidden vectors, as depicted in fig. \ref{fig:classification-restricted-boltzmann-machine-xor}.

The conditional distributions for ClassSetRBM\textsuperscript{XOR} are detailed in Appendix \ref{cha:class-set-rbm-equations}.

\subsection{ClassSetRBM with Redundant Evidence (ClassSetRBM\textsuperscript{OR})}
Sometimes the mutual exclusion constraint is too strong. The same feature may be present in more than one input vector. To handle such scenario, set of  vectors $\textbf{G} = \{\textbf{g}^{(1)}, \dots, \textbf{g}^{(|\textbf{X}|)} \}$ is introduced, as shown in fig. \ref{fig:classification-restricted-boltzmann-machine-or}. Each vector $\boldsymbol{g}^{(s)}$ is a "copy" of respective $\boldsymbol{h}^{(s)}$ from the hidden set $\boldsymbol{H}$. The redundant evidence is imposed with the following constraints:

\begin{equation}
\begin{split}
\sum_{s=1}^{|\boldsymbol{X}|} g_j^{(s)} \in \{0, 1\} &\quad \forall j = 1, \dots, H, \\
h_j^{(s)} \geq g_j^{(s)} &\quad \forall j = 1, \dots, H \quad \forall s = 1, \dots, |\boldsymbol{X}|.
\end{split}
\end{equation}

\input{chapter3/classification-restricted-boltzmann-machine-or-figure}

For ClassSetRBM\textsuperscript{OR} energy function looks as follows:

\begin{equation}
\Energy\left(y, \boldsymbol{X}, \boldsymbol{H}, \boldsymbol{G}; \boldsymbol{\theta}\right) =  -\boldsymbol{d}^\mathsf{T} \boldsymbol{e}_y - \sum_{s} \boldsymbol{b}^\mathsf{T} \boldsymbol{x}^{(s)} - \sum_{s} \boldsymbol{c}^\mathsf{T} \boldsymbol{h}^{(s)} - \sum_{s} \left( {\boldsymbol{h}^{(s)}}^\mathsf{T}\boldsymbol{W}{\boldsymbol{x}^{(s)}} + {\boldsymbol{g}^{(s)}}^\mathsf{T}\boldsymbol{U}\boldsymbol{e}_y \right).
\end{equation}

The target class vector is connected to vectors $\boldsymbol{G}$ through $\boldsymbol{U}$. Matrix $\boldsymbol{W}$ serves as a weight matrix for all layers from $\boldsymbol{H}$.

For ClassSetRBM\textsuperscript{OR} conditional probabilities are included in Appendix \ref{cha:class-set-rbm-equations}.