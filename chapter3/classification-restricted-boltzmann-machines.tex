The Classification Restricted Boltzmann Machine (ClassRBM) is an enhancement of RBM designed specifically for classification problems. Contrary to an RBM, ClassRBM can be applied to a classification problem without the need of using a separate classifier. Because ClassRBM is a self-contained framework, at the same time being simpler than RBM architectures described in Section \ref{sec:class-set-restricted-boltzmann-machines}, this type of RBMs was chosen for solving the author profiling task.

ClassRBM models the distribution of input vector $\boldsymbol{x}$ and target class $y \in \{1, \dots, Y\}$ using a hidden layer of binary stochastic units $\boldsymbol{h}$ \cite{larochelle2012learning}. The target class is encoded with one-hot encoding in a target layer.

We define a probability distribution for a ClassRBM similarly as for typical energy-based models (eq. \ref{eq:energy-based-model-with-visible-and-hidden-distribution}), but we must take into account target class $y$:
\begin{equation}
	P\left(y, \boldsymbol{x}, \boldsymbol{h}; \boldsymbol{\theta}\right) = \frac{e^{-\Energy\left(y, \boldsymbol{x}, \boldsymbol{h}; \boldsymbol{\theta}\right)}} {\Partition\left(\boldsymbol{\theta}\right)},
\end{equation}
where the set of parameters $\boldsymbol{\theta} = \{ \boldsymbol{W}, \boldsymbol{U}, \boldsymbol{b}, \boldsymbol{c}, \boldsymbol{d} \}$. Parameters $\boldsymbol{b}, \boldsymbol{c}, \boldsymbol{W}$ remain exactly the same as in a regular RBM. Two additional parameters $\boldsymbol{U}$, $\boldsymbol{d}$ are defined accordingly:
\begin{itemize}
	\item $\boldsymbol{U} = \left(\begin{smallmatrix}
	U_{11} & U_{12} & \cdots & U_{1Y} \\
	U_{21} & U_{22} & \cdots & U_{2Y} \\
	\vdots  & \vdots  & \ddots & \vdots  \\
	U_{H1} & U_{H2} & \cdots & U_{HY} 
	\end{smallmatrix}\right)$ is a matrix of connection weights between hidden layer and target layer. Element $U_{ik}$ denotes weight of edge connecting nodes $h_{i}$ and $y_{k}$.
	\item $\boldsymbol{d}=\left(d_1, d_2, \dots,d_Y\right)^{\mathsf{T}}$ is a bias vector of target layer, where $Y$ is the number of classes.
\end{itemize}

The architecture of a ClassRBM is presented in fig. \ref{fig:classification-restricted-boltzmann-machine}. Besides elements defined for an RBM, a ClassRBM has an additional (target class) layer, where each node represents one target class. The target class layer and hidden layer form together a fully-connected bipartite graph. The connection weight between the $i$-th element of the hidden layer and the $k$-th element of the target class layer is equal to $U_{ik}$. All the nodes from the target layer are also connected with an additional bias node. The weights of those connections comprise the bias vector of the target class layer. The bias of the $k$-th node in the target class equals $d_k$.

\input{chapter3/classification-restricted-boltzmann-machine-figure}

Energy function resembles the analogous equation for an RBM (eq. \ref{eq:rbm-energy}), but it contains additional terms concerning the target layer:
\begin{equation}
	\Energy\left(y, \boldsymbol{x}, \boldsymbol{h}; \boldsymbol{\theta}\right) =  -\boldsymbol{b}^{\mathsf{T}}\boldsymbol{x} -\boldsymbol{c}^{\mathsf{T}}\boldsymbol{h} -\boldsymbol{h}^{\mathsf{T}}\boldsymbol{W}\boldsymbol{x} -\boldsymbol{d}^{\mathsf{T}}\boldsymbol{e}_y -\boldsymbol{h}^{\mathsf{T}}\boldsymbol{U}\boldsymbol{e}_y,
\end{equation}
with $\boldsymbol{e}_y$ as one-hot vector with $y$-th value set: $\boldsymbol{e}_y = \left( \mathbbm{1}[i=y] \right)_{i=1}^{Y}$.

For ClassRBM the free energy takes the following, tractable form:
\begin{equation}
	\FreeEnergy(y, \boldsymbol{x}; \boldsymbol{\theta}) = -d_y - \sum_{j=1}^{H} \softplus\big(\sum_{i=1}^{X} W_{ji}x_i + U_{jy} + c_j\big).
\end{equation}
