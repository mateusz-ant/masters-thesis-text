The Restricted Boltzmann Machines (RBMs) have been successfully applied to various problems including images classification \cite{hinton2006fast}, speech recognition \cite{mohamed2009deep}, and user's ratings of movies \cite{salakhutdinov2007restricted}. RBMs transform input for the purpose of dimensionality reduction or feature extraction. They typically serve as building blocks of more complex systems. For example, multiple RBMs can be stacked to form a Deep Belief Network.

Formally, the Restricted Boltzmann Machine is a particular form of Boltzmann Machine where lateral connections are forbidden. It consists of two types of units: vector of visible variables $\boldsymbol{x}$ and latent variables $\boldsymbol{h}$. RBM parameters $\boldsymbol{\theta} = \{\boldsymbol{W}, \boldsymbol{b}, \boldsymbol{c}\}$ are defined as follows:
\begin{itemize}
	\item $\boldsymbol{W} = \left(\begin{smallmatrix}
	W_{11} & W_{12} & \cdots & W_{1X} \\
	W_{21} & W_{22} & \cdots & W_{2X} \\
	\vdots  & \vdots  & \ddots & \vdots \\
	W_{H1} & W_{H2} & \cdots & W_{HX} 
	\end{smallmatrix}\right)$ is a weight matrix of connections between visible and hidden layers. Element $W_{ij}$ denotes weight of edge connecting nodes $h_{i}$ and $v_{j}$.
	\item $\boldsymbol{b}=\left(b_1, b_2, \dots,b_X\right)^{\mathsf{T}}$ is a bias vector of visible layer $\boldsymbol{x}$, where $X$ is the number of neurons in the visible layer.
	\item $\boldsymbol{c}=\left(c_1, c_2, \dots,c_H\right)^{\mathsf{T}}$ is a bias vector of hidden layer $\boldsymbol{h}$, where $H$ is the number of neurons in the hidden layer.
\end{itemize}

Restricted Boltzmann Machine can be interpreted as a graph, as shown in fig. \ref{fig:restricted-boltzmann-machine}. The nodes are arranged in two separate layers. Upper (hidden) layer consists of the elements of the latent vector $\boldsymbol{h}$, whereas the nodes in lower (visible) layer represent the elements of the visible vector $\boldsymbol{x}$. Connections between the visible and hidden layers form a complete bipartite graph, i.e., there are no connections within a layer and each node from one layer is connected to all the nodes from the opposite layer. The edge associated between the $i$-th element of the hidden layer and the $j$-th element of the visible layer has weight $W_{ij}$. Besides the visible and hidden layers, there are two additional bias nodes (labeled with $\textit{1}$). Each of those two nodes is connected to all the nodes of one layer. The weights of the edges between the bias nodes and the layers play a role of biases. $b_i$ is the bias for the $i$-th node of the visible layer, and $c_j$ represents the bias for the $j$-th node of the hidden layer, respectively.

\input{chapter3/restricted-boltzmann-machine-figure}

Energy function of RBM configuration $\left(\boldsymbol{x}, \boldsymbol{h}\right)$ is defined as
\begin{equation} \label{eq:rbm-energy}
\Energy\left(\boldsymbol{x}, \boldsymbol{h}\right) = \\ -\boldsymbol{b}^\mathsf{T}\boldsymbol{x} -\boldsymbol{c}^\mathsf{T}\boldsymbol{h} -\boldsymbol{h}^\mathsf{T}\boldsymbol{W}\boldsymbol{x}.
\end{equation}

The term \textit{restricted} means there are no direct dependencies between variables within one layer. This property makes visible and hidden units conditionally independent given the other one:
\begin{equation}
P\left(\boldsymbol{x} \mid \boldsymbol{h}\right)=\prod_{i = 1}^{X} P\left(x_i \mid \boldsymbol{h}\right)
\end{equation}
\begin{equation}
P\left(\boldsymbol{h} \mid \boldsymbol{x}\right)=\prod_{i = 1}^{H} P\left(h_i \mid \boldsymbol{x}\right).
\end{equation}
Common restriction for an RBM is to make visible and hidden units binary, i.e., $\boldsymbol{x} \in \{0,1\}^{X}$, $\boldsymbol{h} \in \{0,1\}^{H}$. In such case the probability that $i$-th latent unit is set to 1 given input vector $\boldsymbol{x}$ becomes:
\begin{equation}
P(h_i = 1 \mid \boldsymbol{x}) = \sigm(c_i + \boldsymbol{W}_{i.}\boldsymbol{x}).
\end{equation}
The probability of a visible unit being set to 1 given latent vector $\boldsymbol{h}$ is expressed analogically:
\begin{equation}
P(x_j = 1 \mid \boldsymbol{h}) = \sigm(b_j + \boldsymbol{W}_{.j}\boldsymbol{h}).
\end{equation}

The free energy is equal to:
\begin{equation} \label{eq:free-energy-rbm}
\FreeEnergy\left(\boldsymbol{x}; \boldsymbol{\theta}\right) = - \boldsymbol{c}^\mathsf{T}\boldsymbol{x} - \sum_{j=1}^{H} \softplus\left(b_j+\boldsymbol{W}_{j\cdot}\boldsymbol{x}\right).
\end{equation}
Unlike the general equation (\ref{eq:free-energy}) for energy-based models, the free energy computation for Restricted Boltzmann Machines does not require iteration over all possible states of the hidden layer. Therefore, the free energy for Restricted Boltzmann Machines is tractable.