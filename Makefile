TEXC=pdflatex
BIBC=bibtex
THESIS=thesis
GARBAGE=*.aux *.log *.bbl *.blg *.dvi *.lof *.toc *.lot *.run.xml thesis.bib thesis.pdf

all: $(THESIS).pdf

$(THESIS).pdf: $(THESIS).tex
	$(TEXC) $(THESIS).tex
	$(BIBC) $(THESIS).aux
	$(TEXC) $(THESIS).tex

.PHONY: clean

clean:
	@rm -f $(GARBAGE)
	@find -type d -name 'chapter*' -exec bash -c "cd {}; rm -f $(GARBAGE)" \;
